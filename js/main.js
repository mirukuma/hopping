function init(){
  var canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");
  ctx.width = 480;
  ctx.height = 360;
  setInterval(tick,1000/60);
  key = new Array();
  sprites = []
  player = new ball(240,30)
  scrollY = 0;
  sprites.push(new bar(240, 180,100))
  sprites.push(player)
  for(var i = 0; i < 9000; i += 100){
    sprites.push(new bar(Math.random()*480,0 - i,100))
  }
}

function tick(){
  ctx.clearRect(0,0,ctx.width,ctx.height);
  for(var i = 0; i < sprites.length; i++) {
    sprites[i].update()
    if(sprites[i].isDestroy){
      sprites.splice(i, 1);
      i -= 1;
    }
  }

}



class sprite{
  constructor(x,y){
    this.isDestroy = false;
    this.name = "sprite"
    this.x = x;
    this.y = y;
  }
  destroy(){
    this.isDestroy = true;
  }
}


class ball extends sprite {
  constructor(x,y){
    super(x,y);
    this.name = "ball"
    this.xSpeed = 0;
    this.ySpeed = 0;
    this.size = 10
  }

  update(){
    this.draw()
    this.move()
    this.touch()
  }

  move(){
    this.ySpeed += 0.1;
    if(key[37]){
      this.xSpeed -= 0.1
    }
    if(key[39]){
      this.xSpeed += 0.1
    }
    if(Math.abs(this.xSpeed) > 10) this.xSpeed = 10 * this.ySpeed / Math.abs(this.ySpeed)
    if(Math.abs(this.ySpeed) > 10) this.ySpeed = 10 * this.ySpeed / Math.abs(this.ySpeed)

    this.x += this.xSpeed
    this.y += this.ySpeed

    if(this.x < -10) this.x = ctx.width + 10
    if(this.x > ctx.width+10) this.x = -10

    if(this.y+scrollY < 0){
      scrollY += 0 - (this.y + scrollY);
    }
    if(this.y+scrollY > ctx.width) this.destroy()
  }

  touch(){
    for(var i = 0; i < sprites.length; i++) {
      if(sprites[i].name == "bar"){
        if((sprites[i].width + this.size) / 2> Math.abs(sprites[i].x - this.x)){
          if((sprites[i].size + this.size) / 2 > Math.abs(sprites[i].y - this.y)){
            this.x -= this.xSpeed;
            this.y -= this.ySpeed;
            this.ySpeed = this.ySpeed * -1.3
          }
        }
      }
    }
  }

  draw(){
    ctx.strokeStyle = "#505050";
    ctx.fillStyle = "#A0A0A0";
    ctx.lineWidth = 2;

    ctx.beginPath();
    ctx.arc(this.x,this.y + scrollY,this.size,0,360);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
  }

}

class bar extends sprite {
  constructor(x,y,width){
    super(x,y);
    this.name = "bar"
    this.width = width;
    this.size = 10
  }
  update(){
    this.draw()
  }
  draw(){
    ctx.strokeStyle = "#505050";
    ctx.fillStyle = "#FFFFFF";
    ctx.lineWidth = 2;

    ctx.fillRect(this.x - this.width / 2, this.y - this.size / 2 + scrollY, this.width,this.size, this.size);
    ctx.strokeRect(this.x - this.width / 2, this.y - this.size / 2 + scrollY, this.width,this.size, this.size);

  }
}



//38 up
//37 left
//39 right
//40 down
